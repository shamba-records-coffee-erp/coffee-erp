package com.shamba.records.edgeservice;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.server.DefaultServerOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.server.WebSessionOAuth2ServerAuthorizationRequestRepository;
import org.springframework.security.web.server.DefaultServerRedirectStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
@Controller
public class MainController {

    private static final String CLIENT_REGISTRATION_ID = "coffee-erp-web-client";

    public Mono<ClientRegistration> clientRegistration;
    private final WebClient webClient;
    private final ReactiveClientRegistrationRepository clientRegistrationRepository;
    private final ReactiveOAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    public MainController(WebClient webClient, ReactiveClientRegistrationRepository clientRegistrationRepository,
                          ReactiveOAuth2AuthorizedClientService oAuth2AuthorizedClientService) {
        this.webClient = webClient;
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.clientRegistration = clientRegistrationRepository.findByRegistrationId(CLIENT_REGISTRATION_ID);
        this.oAuth2AuthorizedClientService = oAuth2AuthorizedClientService;
    }

    @GetMapping("/login")
    public Mono<Void> login(ServerWebExchange exchange) {
        DefaultServerOAuth2AuthorizationRequestResolver authorizationRequestResolver =
                new DefaultServerOAuth2AuthorizationRequestResolver(clientRegistrationRepository);
        WebSessionOAuth2ServerAuthorizationRequestRepository authorizationRequestRepository =
                new WebSessionOAuth2ServerAuthorizationRequestRepository();
        return authorizationRequestResolver.resolve(exchange, CLIENT_REGISTRATION_ID)
                .flatMap(authorizationRequest -> {
                    String authorizationRequestUri = authorizationRequest.getAuthorizationRequestUri();
                    URI redirectUri = UriComponentsBuilder.fromUriString(authorizationRequestUri)
                            .build(true)
                            .toUri();
                    return authorizationRequestRepository.saveAuthorizationRequest(authorizationRequest, exchange)
                            .then(new DefaultServerRedirectStrategy().sendRedirect(exchange, redirectUri));
                });
    }


    @ResponseBody
    @PostMapping("/revoke")
    public Mono<Void> revokeToken(Authentication authentication) {
        OAuth2AuthenticationToken oauthToken = (OAuth2AuthenticationToken) authentication;
        return clientRegistration.flatMap(client -> oAuth2AuthorizedClientService
                .loadAuthorizedClient(client.getRegistrationId(), oauthToken.getName())
                .flatMap(authorizedClient -> webClient.post()
                        .uri("http://127.0.0.1:5000/oauth2/revoke")
                        .headers(httpHeaders -> {
                            httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
                            httpHeaders.setBearerAuth(authorizedClient.getAccessToken().getTokenValue());
                        })
                        .body(BodyInserters.fromFormData("client_id", client.getClientId())
                                .with("client_secret", client.getClientSecret())
                                .with("token", authorizedClient.getAccessToken().getTokenValue())
                        )
                        .retrieve().toBodilessEntity().then(Mono.empty())
                ));
    }
}
