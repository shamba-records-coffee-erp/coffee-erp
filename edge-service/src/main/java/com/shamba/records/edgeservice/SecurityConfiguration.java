package com.shamba.records.edgeservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationEntryPoint;
import org.springframework.security.web.server.authentication.logout.SecurityContextServerLogoutHandler;
import org.springframework.security.web.server.authentication.logout.ServerLogoutHandler;
import org.springframework.security.web.server.authentication.logout.ServerLogoutSuccessHandler;
import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * @author Erick Loningo Lomunyak
 */
@Configuration
public class SecurityConfiguration {
	private static final Logger log = LoggerFactory.getLogger(SecurityConfiguration.class);

	@Bean
    SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
		// @formatter:off
		http
			.cors().and()
			.csrf(csrf -> csrf
					.csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse()))
			.authorizeExchange(authorizeExchange ->
					authorizeExchange
							.pathMatchers("/login", "/assets/**", "/favicon.ico").permitAll()
							.anyExchange().authenticated())
			.logout(logout -> logout
					.logoutHandler(serverLogoutHandler())
					.logoutSuccessHandler(serverLogoutSuccessHandler()))
			.exceptionHandling(exceptionHandling ->
					exceptionHandling
							.authenticationEntryPoint(new RedirectServerAuthenticationEntryPoint("/login"))
			)
			.oauth2Login();

		// @formatter:on
		return http.build();
	}

	ServerLogoutHandler serverLogoutHandler() {
		return (exchange, authentication) -> new SecurityContextServerLogoutHandler().logout(exchange, authentication)
				.then(deleteCookies(exchange.getExchange().getRequest(), exchange.getExchange().getResponse()));
	}

	public Mono<Void> deleteCookies(ServerHttpRequest request, ServerHttpResponse response) {
		MultiValueMap<String, HttpCookie> cookies = request.getCookies();

		for (Map.Entry<String, List<HttpCookie>> cookie : cookies.entrySet()) {
			for (HttpCookie cookieToBeDeleted : cookie.getValue()) {
				log.debug("Deleting cookie: {} having value: {}", cookieToBeDeleted.getName(), cookieToBeDeleted
						.getValue());
				response.addCookie(ResponseCookie.from(cookieToBeDeleted.getName(), cookieToBeDeleted.getValue())
						.maxAge(0).build());
			}
		}

		log.debug("Response cookies" + response.getCookies().toString());
		return Mono.empty();
	}

	ServerLogoutSuccessHandler serverLogoutSuccessHandler() {
		return (exchange, authentication) -> Mono.empty();
	}
}
