package com.shamba.records.edgeservice;

import io.netty.handler.logging.LogLevel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

/**
 * @author Erick Loningo Lomunyak
 */
@Configuration
public class WebClientConfig {

    @Bean("webClient")
    WebClient webClient(WebClient.Builder builder) {
        HttpClient httpClient = HttpClient
                .create()
                .wiretap("reactor.netty.http.client.HttpClient",
                        LogLevel.DEBUG, AdvancedByteBufFormat.TEXTUAL);
        return builder
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
    }
}
