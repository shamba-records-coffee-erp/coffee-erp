package com.shamba.records.farmermanagementservice;

import com.shamba.records.farmermanagementservice.configs.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EnableConfigurationProperties({LiquibaseProperties.class, ApplicationProperties.class})
public class FarmerManagementServiceApplication {

	public static final String ID = "FMS_API";

	public static void main(String[] args) {
		SpringApplication.run(FarmerManagementServiceApplication.class, args);
	}

}
