package com.shamba.records.farmermanagementservice.service;

/**
 * @author Erick Loningo Lomunyak
 */
public interface BrokerMessageService<T> {

    public void consume(T payload);
}
