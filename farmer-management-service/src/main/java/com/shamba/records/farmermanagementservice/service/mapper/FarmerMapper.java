package com.shamba.records.farmermanagementservice.service.mapper;

import com.shamba.records.farmermanagementservice.model.Farmer;
import com.shamba.records.farmermanagementservice.service.dto.FarmerDTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

/**
 * @author Erick Loningo Lomunyak
 */
@Service("farmerMapper")
@Mapper(componentModel = "spring")
public interface FarmerMapper {

    FarmerDTO toDTO(Farmer farmer);

    Farmer toEntity(FarmerDTO dto);
}
