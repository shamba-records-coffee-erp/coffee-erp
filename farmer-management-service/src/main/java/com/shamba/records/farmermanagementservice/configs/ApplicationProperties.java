package com.shamba.records.farmermanagementservice.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Erick Loningo Lomunyak
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
}
