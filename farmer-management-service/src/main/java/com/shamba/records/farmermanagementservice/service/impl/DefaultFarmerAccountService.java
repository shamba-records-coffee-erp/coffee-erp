package com.shamba.records.farmermanagementservice.service.impl;

import com.shamba.records.farmermanagementservice.model.Farmer;
import com.shamba.records.farmermanagementservice.repository.FarmerRepository;
import com.shamba.records.farmermanagementservice.service.AccountService;
import com.shamba.records.farmermanagementservice.service.dto.FarmerDTO;
import com.shamba.records.farmermanagementservice.service.exception.CoopMemberNoAlreadyExistsException;
import com.shamba.records.farmermanagementservice.service.exception.FarmerAlreadyExistsException;
import com.shamba.records.farmermanagementservice.service.exception.IdNoAlreadyExistsException;
import com.shamba.records.farmermanagementservice.service.exception.PhoneAlreadyExistsException;
import com.shamba.records.farmermanagementservice.service.mapper.FarmerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

/**
 * @author Erick Loningo Lomunyak
 */
@Transactional
@Service("dfAccountService")
public class DefaultFarmerAccountService  implements AccountService {

    private static final Logger log = LoggerFactory.getLogger(DefaultFarmerAccountService.class);

    private final FarmerMapper farmerMapper;
    private final FarmerRepository farmerRepository;

    public DefaultFarmerAccountService(FarmerMapper farmerMapper, FarmerRepository farmerRepository) {
        this.farmerMapper = farmerMapper;
        this.farmerRepository = farmerRepository;
    }

    @Override
    public FarmerDTO createFarmer(Farmer farmer) {
        log.info("Request to save add new farmer:: {}", farmer);
        farmerRepository.findFirstByEmailIgnoreCase(farmer.getEmail()).ifPresent(existingFarmer -> {
            boolean removed = removeNonActivatedFarmer(existingFarmer);
            if (!removed) {
                throw new FarmerAlreadyExistsException(existingFarmer.getEmail());
            }
        });

        farmerRepository.findFirstByPhoneNumber(farmer.getPhoneNumber()).ifPresent(existingFarmer -> {
            boolean removed = removeNonActivatedFarmer(existingFarmer);
            if (!removed) {
                throw new PhoneAlreadyExistsException(existingFarmer.getPhoneNumber());
            }
        });

        farmerRepository.findFirstByIdNo(farmer.getIdNo()).ifPresent(existingFarmer -> {
            boolean removed = removeNonActivatedFarmer(existingFarmer);
            if (!removed) {
                throw new IdNoAlreadyExistsException(existingFarmer.getIdNo());
            }
        });

        if (!Objects.isNull(farmer.getCoopId()) && !Objects.isNull(farmer.getCoopMemberNo())) {
            farmerRepository.findFirstByCoopIdAndCoopMemberNo(farmer.getCoopId(), farmer.getCoopMemberNo())
                    .ifPresent(existingFarmer -> {
                        boolean removed = removeNonActivatedFarmer(existingFarmer);
                        if (!removed) {
                            throw new CoopMemberNoAlreadyExistsException(existingFarmer.getCoopMemberNo());
                        }
                    });
        }

        return farmerMapper.toDTO(farmerRepository.save(farmer));
    }

    @Override
    public boolean removeNonActivatedFarmer(Farmer farmer) {
        if (farmer.isActivated()) {
            return false;
        }
        farmerRepository.delete(farmer);
        farmerRepository.flush();
        return true;
    }
}
