package com.shamba.records.farmermanagementservice.service.impl;

import com.shamba.records.farmermanagementservice.FarmerManagementServiceApplication;
import com.shamba.records.farmermanagementservice.service.AccountService;
import com.shamba.records.farmermanagementservice.service.BrokerMessageService;
import com.shamba.records.farmermanagementservice.service.dto.FarmerDTO;
import com.shamba.records.farmermanagementservice.service.mapper.FarmerMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * @author Erick Loningo Lomunyak
 */
@Service("farmerBrokerMessageService")
public class FarmerBrokerMessageService implements BrokerMessageService<FarmerDTO> {

    private final AccountService dfAccountService;
    private final FarmerMapper farmerMapper;

    public FarmerBrokerMessageService(AccountService dfAccountService, FarmerMapper farmerMapper) {
        this.dfAccountService = dfAccountService;
        this.farmerMapper = farmerMapper;
    }

    @KafkaListener(groupId = FarmerManagementServiceApplication.ID,
            autoStartup = "true",
            topics = {
                    "PutRecord-Farmer-v1"
            },
            concurrency = "1",
            containerFactory = "farmerDTOLcf")
    @Override
    public void consume(FarmerDTO payload) {
        dfAccountService.createFarmer(farmerMapper.toEntity(payload));
    }
}
