package com.shamba.records.farmermanagementservice.model.enums;

import java.util.Objects;

public enum Gender {
    Male, Female
}
