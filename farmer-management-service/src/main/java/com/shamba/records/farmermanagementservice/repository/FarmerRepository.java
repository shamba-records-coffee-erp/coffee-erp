package com.shamba.records.farmermanagementservice.repository;

import com.shamba.records.farmermanagementservice.model.Farmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Erick Loningo Lomunyak
 */
@Repository
public interface FarmerRepository extends JpaRepository<Farmer, Long> {

    Optional<Farmer> findFirstByEmailIgnoreCase(String email);
    Optional<Farmer> findFirstByPhoneNumber(String phone);
    Optional<Farmer> findFirstByIdNo(String idNo);
    Optional<Farmer> findFirstByCoopIdAndCoopMemberNo(long coopId, String coopMemberNo);
}
