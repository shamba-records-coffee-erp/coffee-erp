package com.shamba.records.farmermanagementservice.service.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
public class CoopMemberNoAlreadyExistsException extends AbstractThrowableProblem {
    private static final URI TYPE = URI.create("https://coffee-erp.org/coop-member-no-already-exists");

    public CoopMemberNoAlreadyExistsException(String coopMemberNo) {
        super(TYPE, "Cooperative member number already used!", Status.CONFLICT,
                String.format("Error: Cooperative member number '%s' is already is use", coopMemberNo));
    }
}
