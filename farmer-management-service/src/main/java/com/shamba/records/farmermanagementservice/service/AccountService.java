package com.shamba.records.farmermanagementservice.service;

import com.shamba.records.farmermanagementservice.model.Farmer;
import com.shamba.records.farmermanagementservice.service.dto.FarmerDTO;

/**
 * @author Erick Loningo Lomunyak
 */
public interface AccountService {

    FarmerDTO createFarmer(Farmer farmer);

    boolean removeNonActivatedFarmer(Farmer farmer);
}
