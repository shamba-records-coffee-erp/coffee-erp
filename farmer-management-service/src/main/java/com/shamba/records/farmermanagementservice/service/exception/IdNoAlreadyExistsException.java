package com.shamba.records.farmermanagementservice.service.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
public class IdNoAlreadyExistsException extends AbstractThrowableProblem {
    private static final URI TYPE = URI.create("https://coffee-erp.org/idno-already-exists");

    public IdNoAlreadyExistsException(String idNo) {
        super(TYPE, "ID or Passport number already used!", Status.CONFLICT,
                String.format("Error: ID or Passport number '%s' is already is use", idNo));
    }
}
