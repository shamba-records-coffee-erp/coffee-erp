package com.shamba.records.farmermanagementservice.service.dto;

import javax.validation.constraints.NotNull;

/**
 * @author Erick Loningo Lomunyak
 * @date 12/12/2021-12:15 AM
 */
public class FarmerDTO extends UserDTO {

    @NotNull
    private Long userId;

    private long coopId;

    private String coopMemberNo;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getCoopId() {
        return coopId;
    }

    public void setCoopId(long coopId) {
        this.coopId = coopId;
    }

    public String getCoopMemberNo() {
        return coopMemberNo;
    }

    public void setCoopMemberNo(String coopMemberNo) {
        this.coopMemberNo = coopMemberNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FarmerDTO farmerDTO = (FarmerDTO) o;
        return userId == farmerDTO.userId;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "FarmerDTO{" +
                "userId=" + userId +
                ", coopId=" + coopId +
                ", coopMemberNo='" + coopMemberNo + '\'' +
                "} " + super.toString();
    }
}
