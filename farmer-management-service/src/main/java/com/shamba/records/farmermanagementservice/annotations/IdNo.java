package com.shamba.records.farmermanagementservice.annotations;

import org.springframework.core.annotation.AliasFor;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Erick Loningo Lomunyak
 * @date 11/12/2021-11:40 PM
 */
@Target({
        ElementType.METHOD,
        ElementType.FIELD,
        ElementType.TYPE,
        ElementType.TYPE_USE,
})
@ReportAsSingleViolation
@Retention(RetentionPolicy.RUNTIME)
@Pattern(regexp = "\\A\\d{4,10}\\z")
@Documented
@Constraint(validatedBy = {})
public @interface IdNo {

    @AliasFor(attribute = "message")
    public String value() default "Invalid national Id number";

    public String message() default "Invalid national Id number";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};

}
