package com.shamba.records.farmermanagementservice.configs;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shamba.records.farmermanagementservice.service.dto.FarmerDTO;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Map;

/**
 * @author Erick Loningo Lomunyak
 */
@Configuration
public class KafkaConfig {

    private final KafkaProperties kafkaProperties;

    public KafkaConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Bean("farmerDTOCf")
    public ConsumerFactory<String, FarmerDTO> farmerDTOCf(ObjectMapper objectMapper) {
        TypeReference<FarmerDTO> tr = new TypeReference<FarmerDTO>() {};
        Deserializer<FarmerDTO> vder = new ErrorHandlingDeserializer<>(new JsonDeserializer<>(tr, objectMapper));
        Map<String, Object> props = kafkaProperties.buildConsumerProperties();
        vder.configure(props, false);
        Deserializer<String> kder = new ErrorHandlingDeserializer<>(new StringDeserializer());
        kder.configure(props, true);
        return new DefaultKafkaConsumerFactory<>(props, kder, vder);
    }

    @Bean("farmerDTOLcf")
    public ConcurrentKafkaListenerContainerFactory<String, FarmerDTO> farmerDTOLcf(
            ConsumerFactory<String, FarmerDTO> farmerDTOCf) {

        ConcurrentKafkaListenerContainerFactory<String, FarmerDTO> factory;
        factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(farmerDTOCf);
        factory.setCommonErrorHandler(new DefaultErrorHandler());
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
        return factory;
    }

}
