package com.shamba.records.farmermanagementservice.service.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
public class PhoneAlreadyExistsException extends AbstractThrowableProblem {
    private static final URI TYPE = URI.create("https://coffee-erp.org/phone-already-exists");

    public PhoneAlreadyExistsException(String phone) {
        super(TYPE, "Phone number already used!", Status.CONFLICT,
                String.format("Error: phone number '%s' is already is use", phone));
    }
}
