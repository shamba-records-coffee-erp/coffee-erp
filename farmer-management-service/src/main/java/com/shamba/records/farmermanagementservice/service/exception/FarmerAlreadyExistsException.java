package com.shamba.records.farmermanagementservice.service.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
public class FarmerAlreadyExistsException extends AbstractThrowableProblem {
    private static final URI TYPE = URI.create("https://coffee-erp.org/email-already-exists");

    public FarmerAlreadyExistsException(String email) {
        super(TYPE, "Email already used!", Status.CONFLICT,
                String.format("Error: Email '%s' is already is use", email));
    }
}
