package com.shamba.records.farmermanagementservice.model;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * @author Erick Loningo Lomunyak
 * @date 11/12/2021-8:36 PM
 */
@Entity
@Table(name = "farmer", indexes = {
        @Index(columnList = "idNo", unique = true, name = "erp_users_id_no_index"),
        @Index(columnList = "email", unique = true, name = "erp_users_email_index"),
        @Index(columnList = "phoneNumber", unique = true, name = "erp_users_phone_number_index"),
})
public class Farmer extends User {

    @NotNull
    private Long userId;

    private Long coopId;

    private String coopMemberNo;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCoopId() {
        return coopId;
    }

    public void setCoopId(Long coopId) {
        this.coopId = coopId;
    }

    public String getCoopMemberNo() {
        return coopMemberNo;
    }

    public void setCoopMemberNo(String coopMemberNo) {
        this.coopMemberNo = coopMemberNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Farmer farmer = (Farmer) o;
        return Objects.equals(userId, farmer.userId) &&
                Objects.equals(coopId, farmer.coopId) &&
                Objects.equals(coopMemberNo, farmer.coopMemberNo);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


    @Override
    public String toString() {
        return "Farmer{" +
                "userId=" + userId +
                ", coopId=" + coopId +
                ", coopMemberNo='" + coopMemberNo + '\'' +
                "} " + super.toString();
    }
}
