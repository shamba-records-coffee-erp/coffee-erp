package com.shamba.records.farmermanagementservice.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @author Erick Loningo Lomunyak
 */
@RestController
@RequestMapping("/farmers/user")
public class UserResource {

    @GetMapping("info")
    public Principal getInfo(Principal principal) {
        return principal;
    }
}
