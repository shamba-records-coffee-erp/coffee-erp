package com.shamba.records.accountservice.web;

import com.shamba.records.accountservice.dto.FarmerDTO;
import com.shamba.records.accountservice.dto.FarmerRegistrationDTO;
import com.shamba.records.accountservice.service.FarmerAccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Erick Loningo Lomunyak
 */
@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final FarmerAccountService farmerAccountService;

    public AccountController(FarmerAccountService farmerAccountService) {
        this.farmerAccountService = farmerAccountService;
    }

    @PostMapping("/farmer")
    public ResponseEntity<FarmerDTO> createNewFarmer(
            @RequestBody FarmerRegistrationDTO farmer) {
        FarmerDTO farmerDTO = farmerAccountService.createFarmer(farmer);
        return ResponseEntity.status(HttpStatus.CREATED).body(farmerDTO);
    }

}
