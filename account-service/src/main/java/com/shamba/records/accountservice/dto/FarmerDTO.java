package com.shamba.records.accountservice.dto;

import com.shamba.records.accountservice.enums.Gender;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * @author Erick Loningo Lomunyak
 */
public class FarmerDTO extends UserDTO {

    private Long userId;
    private Long coopId;
    private String coopMemberNo;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCoopId() {
        return coopId;
    }

    public void setCoopId(Long coopId) {
        this.coopId = coopId;
    }

    public String getCoopMemberNo() {
        return coopMemberNo;
    }

    public void setCoopMemberNo(String coopMemberNo) {
        this.coopMemberNo = coopMemberNo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FarmerDTO farmerDTO = (FarmerDTO) o;
        return Objects.equals(userId, farmerDTO.userId) &&
                Objects.equals(coopId, farmerDTO.coopId) &&
                Objects.equals(coopMemberNo, farmerDTO.coopMemberNo);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "FarmerDTO{" +
                "userId=" + userId +
                ", coopId='" + coopId + '\'' +
                ", coopMemberNo='" + coopMemberNo + '\'' +
                "} " + super.toString();
    }
}
