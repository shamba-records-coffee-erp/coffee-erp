package com.shamba.records.accountservice.enums;

import java.util.Objects;

/**
 * @author Erick Loningo Lomunyak
 */
public enum Gender {
    Male, Female
}
