package com.shamba.records.accountservice.annotations;

import org.springframework.core.annotation.AliasFor;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Erick Loningo Lomunyak
 * @date 11/12/2021-11:39 PM
 */
@Target({
        ElementType.METHOD,
        ElementType.FIELD,
        ElementType.PARAMETER,
        ElementType.TYPE_USE
})
@Retention(value = RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Pattern(regexp = "\\A\\d{9,14}\\z")
@Documented
@Constraint(validatedBy = {})
public @interface Msisdn {

    @AliasFor(attribute = "message")
    public String value() default "Invalid phone number";

    public String message() default "Invalid phone number";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};
}

