package com.shamba.records.accountservice.dto;

import com.shamba.records.accountservice.enums.Gender;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Erick Loningo Lomunyak
 */
public class FarmerRegistrationDTO extends UserRegistrationDTO {

    private Long userId;
    private String coopId;
    private String coopMemberNo;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCoopId() {
        return coopId;
    }

    public void setCoopId(String coopId) {
        this.coopId = coopId;
    }

    public String getCoopMemberNo() {
        return coopMemberNo;
    }

    public void setCoopMemberNo(String coopMemberNo) {
        this.coopMemberNo = coopMemberNo;
    }

    @Override
    public String toString() {
        return "FarmerRegistrationDTO{" +
                "userId=" + userId +
                ", coopId='" + coopId + '\'' +
                ", coopMemberNo='" + coopMemberNo + '\'' +
                "} " + super.toString();
    }
}
