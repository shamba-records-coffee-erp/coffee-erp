package com.shamba.records.accountservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;

/**
 * @author Erick Loningo Lomunyak
 */

@Configuration
public class LoadBalancerConfig {
    private static final Logger log = LoggerFactory.getLogger(LoadBalancerConfig.class);

    @Bean
    public ServiceInstanceListSupplier defaultServiceInstanceListSupplier() {
        log.info("Configuring Load balancer to prefer same instance");
        return new DefaultServiceInstanceListSupplier("auth-service");
    }
}

class DefaultServiceInstanceListSupplier implements ServiceInstanceListSupplier {

    private final String serviceId;

    DefaultServiceInstanceListSupplier(String serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public String getServiceId() {
        return serviceId;
    }

    @Override
    public Flux<List<ServiceInstance>> get() {
        return Flux.just(Arrays.asList(
                new DefaultServiceInstance(serviceId + "1", serviceId, "127.0.0.1", 5000, false)
        ));
    }
}
