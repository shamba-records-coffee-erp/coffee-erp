package com.shamba.records.accountservice.enums;

public enum UserType {
    SystemAdmin, Farmer, Staff, CoopUser, CoopAdmin, CoopClerk, CoopStaff,
    WareHouseUser, WareHouseAdmin, WareHouseClerk, WareHouseStaff
}
