package com.shamba.records.accountservice.service.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.net.URI;
import java.util.Map;

/**
 * @author Erick Loningo Lomunyak
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Problem {

    private URI type;
    private String title;
    private String detail;
    private Map<String, Object> parameters;

    public URI getType() {
        return type;
    }

    public void setType(URI type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
