package com.shamba.records.accountservice.service.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.StatusType;

import javax.annotation.Nullable;
import java.net.URI;

/**
 * @author Erick Loningo Lomunyak
 */
public class DefaultErrorHandler extends AbstractThrowableProblem {

    public DefaultErrorHandler(@Nullable URI type, @Nullable String title, @Nullable StatusType status,
                               @Nullable String detail) {
        super(type, title, status, detail);
    }
}
