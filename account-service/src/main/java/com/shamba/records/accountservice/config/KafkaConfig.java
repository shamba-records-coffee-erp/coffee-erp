package com.shamba.records.accountservice.config;

import com.shamba.records.accountservice.dto.FarmerDTO;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

/**
 * @author Erick Loningo Lomunyak
 */
@Configuration
public class KafkaConfig {

    private final KafkaProperties kafkaProperties;

    public KafkaConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Bean("farmerDTOPf")
    public ProducerFactory<String, FarmerDTO> farmerDTOPf() {
        return new DefaultKafkaProducerFactory<>(kafkaProperties.buildProducerProperties());
    }

    @Bean("farmerDTORs")
    public KafkaTemplate<String, FarmerDTO> farmerDTORs(ProducerFactory<String, FarmerDTO> farmerDTOPf) {
        return new KafkaTemplate<>(farmerDTOPf);
    }

}
