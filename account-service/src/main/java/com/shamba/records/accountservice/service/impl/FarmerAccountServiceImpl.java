package com.shamba.records.accountservice.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shamba.records.accountservice.dto.FarmerDTO;
import com.shamba.records.accountservice.dto.FarmerRegistrationDTO;
import com.shamba.records.accountservice.dto.UserDTO;
import com.shamba.records.accountservice.mapper.FarmerMapper;
import com.shamba.records.accountservice.service.FarmerAccountService;
import com.shamba.records.accountservice.service.exception.DefaultErrorHandler;
import com.shamba.records.accountservice.service.exception.Problem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.zalando.problem.Status;
import reactor.core.publisher.Mono;

/**
 * @author Erick Loningo Lomunyak
 */

@Service("farmerAccountService")
public class FarmerAccountServiceImpl implements FarmerAccountService {

    private static final Logger log = LoggerFactory.getLogger(FarmerAccountServiceImpl.class);
    private static final String FARMER_REG_DEST = "PutRecord-Farmer-v1";

    private final WebClient webClient;
    private final FarmerMapper farmerMapper;
    private final KafkaTemplate<String, FarmerDTO> farmerDTORs;

    public FarmerAccountServiceImpl(@Qualifier("clientCredentialsWebClient") WebClient webClient, FarmerMapper farmerMapper,
                                    KafkaTemplate<String, FarmerDTO> farmerDTORs) {
        this.webClient = webClient;
        this.farmerMapper = farmerMapper;
        this.farmerDTORs = farmerDTORs;
    }

    private static Mono<? extends Throwable> handleError(String error) {
        try {
            Problem problem = new ObjectMapper().readValue(error, Problem.class);
            return Mono.error(new DefaultErrorHandler(
                    problem.getType(),
                    problem.getTitle(),
                    Status.BAD_REQUEST,
                    problem.getDetail()));
        } catch (JsonProcessingException e) {
            return Mono.error(new RuntimeException(e.getMessage()));
        }

    }

    @Override
    public FarmerDTO createFarmer(FarmerRegistrationDTO registrationDTO ) {
        UserDTO userDTO = webClient.post()
                .uri("http://auth-service/auth/account")
                .body(BodyInserters.fromPublisher(Mono.just(registrationDTO), FarmerRegistrationDTO.class))
                .retrieve()
                .onStatus(HttpStatus::isError,
                        clientResponse -> clientResponse.bodyToMono(String.class)
                                .flatMap(FarmerAccountServiceImpl::handleError))
                .bodyToMono(UserDTO.class).block();
        log.info("user created :: {}", userDTO);
        Assert.notNull(userDTO, "user registration failed!!!");

        // register farmer in farmer management service
        FarmerDTO farmerDTO = farmerMapper.toFarmerDTO(registrationDTO);
        farmerDTO.setUserId(userDTO.getId());
        log.info("request to register farmer :: {}", farmerDTO);
        farmerDTORs.send(FARMER_REG_DEST, farmerDTO);
        return farmerDTO;
    }
}
