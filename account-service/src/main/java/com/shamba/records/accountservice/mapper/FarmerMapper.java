package com.shamba.records.accountservice.mapper;

import com.shamba.records.accountservice.dto.FarmerDTO;
import com.shamba.records.accountservice.dto.FarmerRegistrationDTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author Erick Loningo Lomunyak
 */
@Component
@Mapper(componentModel = "spring")
public interface FarmerMapper {

    FarmerDTO toFarmerDTO(FarmerRegistrationDTO farmerRegistrationDTO);
}
