package com.shamba.records.accountservice.service;

import com.shamba.records.accountservice.dto.FarmerDTO;
import com.shamba.records.accountservice.dto.FarmerRegistrationDTO;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;

/**
 * @author Erick Loningo Lomunyak
 */
public interface FarmerAccountService {

    FarmerDTO createFarmer(FarmerRegistrationDTO farmer);
}
