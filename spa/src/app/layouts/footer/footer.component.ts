import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'erp-footer',
  templateUrl: './footer.component.html',
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  date!: Date;

  constructor() { }

  ngOnInit(): void {
    this.date = new Date();
  }

}
