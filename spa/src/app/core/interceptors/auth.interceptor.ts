import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Injectable, isDevMode} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {LocalStorageService} from 'ngx-webstorage';
import {throwError} from 'rxjs';
import {AccountService} from '../auth/account.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private $localStorageService: LocalStorageService,
    private router: Router
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const accessToken = this.authService.getAccessToken();
    const refreshToken = this.authService.getRefreshToken();

    if (accessToken) {
      req = req.clone( {
        setHeaders: {
          Authorization: 'Bearer ' + accessToken
        }
      });
    }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        setHeaders: {
          'content-type': 'application/json'
        }
      });
    }

    req = req.clone({
      headers: req.headers.set('Accept', 'application/json')
    });

    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401 && err.url && !err.url.includes('auth/account') && !err.url.includes('oauth2/token')) {
          if (isDevMode()) console.log(err);
          if (err.error.error === 'invalid_token' && refreshToken) {
            this.authService.reAuthenticate(refreshToken)
              .subscribe(() => {
                location.reload();
              }, _ =>  {
                this.router.navigate(['login']).then(_ => {
                  if (isDevMode()) console.info('user not authenticated!');
                });
              });
          } else {
            this.router.navigate(['login']).then(_ => {
              if (isDevMode()) console.info('user not authenticated!');
            });
          }
        }
        return throwError(() => err);
      })
    );
  }

}
