import * as dayjs from 'dayjs';

export interface Authority {
  name: string;
}

export interface Account {
  id: string | null;
  firstName: string | null;
  middleName: string | null;
  lastName: string | null;
  email: string | null;
  idNo: string | null;
  phoneNumber: string | null;
  address: string | null;
  gender: string | null;
  dob: dayjs.Dayjs | null;
  activated: boolean | null;
  roles:  Authority[] | [];
}
