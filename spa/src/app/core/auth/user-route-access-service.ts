import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable, isDevMode} from '@angular/core';
import {AccountService} from './account.service';
import {StateStorageService} from './state-storage.service';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class  UserRouteAccessService implements CanActivate{

  constructor(
    private router: Router, private accountService: AccountService, private stateStorageService: StateStorageService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.accountService.identity().pipe(
      map((account) => {
        if (account) {
          const authorities = route.data['authorities'];

          if (!authorities || authorities.length === 0 || this.accountService.hasAnyAuthority(authorities)) {
            return true;
          }

          if (isDevMode()) {
            console.error('User has not any of required authorities: ', authorities);
          }
          this.router.navigate(['/app/home']).then();
          return false;
        }

        this.stateStorageService.storeUrl(state.url);
        window.location.replace('http://127.0.0.1:8000');
        return false;
      })
    );
  }
}
