import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Observable, of, ReplaySubject} from 'rxjs';
import {Account} from './account.model';
import * as dayjs from 'dayjs';
import {catchError, mergeMap, shareReplay, tap} from 'rxjs/operators';
import {StateStorageService} from './state-storage.service';
import {Router} from '@angular/router';
import {LocalStorageService} from 'ngx-webstorage';

export const ACCOUNT_INFO_URL = '/auth/account/info';
export const LOGOUT_URL = '/logout';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private userIdentity: Account | null = null;
  private authenticationState = new ReplaySubject<Account | null>(1);
  private accountCache$?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router,
    private localStorageService: LocalStorageService,
    private $stateStorageService: StateStorageService
  ) { }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
    this.authenticationState.next(this.userIdentity);
  }

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache$ || force || !this.isAuthenticated()) {
      this.accountCache$ = this.fetch().pipe(
        catchError(() => of(null)),
        tap((account: Account | null) => {
          this.authenticate(account);
        }),
        shareReplay()
      );
    }
    return this.accountCache$;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  fetch(): Observable<Account> {
    return this.http.get<Account>(ACCOUNT_INFO_URL)
      .pipe(
        map((res) => {
          if (res) {
            res.dob = res.dob ? dayjs(res.dob) : null;
          }
          return res;
        })
      )
  }

  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.isAuthenticated()) {
      return false;
    }
    if (!Array.isArray(authorities)) {
      authorities = [authorities];
    }
    return authorities.some(
      (authority) => this.userIdentity?.roles.map((role) => role.name).includes(authority));
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  navigateToStoredUrl() {
    const previousUrl = this.$stateStorageService.getUrl();
    if (previousUrl) {
      this.$stateStorageService.clearUrl();
      this.router.navigate(previousUrl as any).then();
    }
  }

  logout(): Observable<any> {
    return of(this.authenticate(null)).pipe(
      mergeMap(() => this.http.post('/revoke', {responseType: 'text'})),
      mergeMap(() => this.http.post(LOGOUT_URL, {}, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
        responseType: 'text'
      }))
    );
  }
}
