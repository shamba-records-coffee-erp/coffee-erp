import { NgModule } from '@angular/core';
import {SharedLibModule} from './shared-lib.module';
import {SharedPrimengModule} from './shared-primeng.module';

@NgModule({
  imports: [
    SharedLibModule,
    SharedPrimengModule
  ],
  exports: [
    SharedLibModule,
    SharedPrimengModule,
  ]
})
export class SharedModule { }
