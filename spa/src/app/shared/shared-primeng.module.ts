import { NgModule } from '@angular/core';
import { ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToastModule} from 'primeng/toast';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {MegaMenuModule} from 'primeng/megamenu';
import {MessageModule} from 'primeng/message';
import {BreadcrumbModule} from 'primeng/breadcrumb';

@NgModule({
  exports: [
    ToastModule,
    MessageModule,
    MegaMenuModule,
    ConfirmPopupModule,
    ConfirmDialogModule,
    BreadcrumbModule
  ]
})
export class SharedPrimengModule { }
