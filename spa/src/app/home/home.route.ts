import {Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {UserRouteAccessService} from '../core/auth/user-route-access-service';

export const homeRoute: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [UserRouteAccessService],
    data: {
      breadcrumb: 'Home'
    }
  }
];


