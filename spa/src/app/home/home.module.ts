import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';
import {homeRoute} from './home.route';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(homeRoute),
    SharedModule
  ],
})
export class HomeModule { }
