import { NgModule } from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {coopRoutes} from './coop.routes';
import {CoopListComponent} from './coop-list/coop-list.component';


@NgModule({
  declarations: [
    CoopListComponent
  ],
  imports: [
    SharedModule, RouterModule.forChild(coopRoutes)
  ]
})
export class CoopModule { }
