import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoopListComponent } from './coop-list.component';

describe('CoopListComponent', () => {
  let component: CoopListComponent;
  let fixture: ComponentFixture<CoopListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoopListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoopListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
