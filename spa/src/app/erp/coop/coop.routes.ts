import {Routes} from '@angular/router';
import {CoopListComponent} from './coop-list/coop-list.component';
import {Authority} from '../../config/authority.constants';

export const coopRoutes: Routes = [
  {
    path: '',
    component: CoopListComponent,
    data: {
      authorities: [
        Authority.SYSTEM_ADMIN,
        Authority.COOP_USER,
        Authority.COOP_CLERK
      ],
      breadcrumb: 'Cooperative'
    },
  }
];
