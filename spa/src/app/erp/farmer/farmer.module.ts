import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {farmerRoutes} from './farmer.route';
import {SharedModule} from '../../shared/shared.module';
import { FarmerListComponent } from './farmer-list/farmer-list.component';

@NgModule({
  declarations: [
    FarmerListComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(farmerRoutes),
  ]
})
export class FarmerModule { }
