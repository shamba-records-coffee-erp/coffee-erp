import {Routes} from '@angular/router';
import {Authority} from '../../config/authority.constants';
import {FarmerListComponent} from './farmer-list/farmer-list.component';

export const farmerRoutes: Routes = [
  {
    path: '',
    component: FarmerListComponent,
    data: {
      authorities: [
        Authority.SYSTEM_ADMIN,
        Authority.FARMER
      ],
      breadcrumb: 'Farmer'
    },
  }
]
