import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserRouteAccessService} from '../core/auth/user-route-access-service';

export const erpRoutes: Routes = [
  {
    path: '',
    canActivate: [UserRouteAccessService],
    children: [
      {
        path: 'farmer',
        loadChildren: () => import('./farmer/farmer.module').then(m => m.FarmerModule)
      },
      {
        path: 'coop',
        loadChildren: () => import('./coop/coop.module').then(m => m.CoopModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(erpRoutes)]
})
export class ErpRoutingModule { }
