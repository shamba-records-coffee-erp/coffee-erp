package com.shamba.records.authservice.model.enums;

public enum UserType {
    System, SystemAdmin, Farmer, Staff, CoopUser, CoopAdmin, CoopClerk, CoopStaff,
    WareHouseUser, WareHouseAdmin, WareHouseClerk, WareHouseStaff
}
