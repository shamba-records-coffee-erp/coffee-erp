package com.shamba.records.authservice.web.rest;

import com.shamba.records.authservice.service.UserService;
import com.shamba.records.authservice.service.dto.UserDTO;
import com.shamba.records.authservice.service.vm.UserVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/auth")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/account")
    public ResponseEntity<UserDTO> addUser(@Valid @RequestBody UserVM user) {
        log.info("adding new User {}", user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
    }

    @GetMapping("/account")
    public ResponseEntity<List<UserDTO>> getUsers(Pageable pageable) {
        Page<UserDTO> page = userService.getAllUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        UserDTO existingUser = userService.getUserById(id);
        return Objects.isNull(existingUser) ? ResponseEntity.notFound().build() : ResponseEntity.ok(existingUser);
    }

    @GetMapping("/account/info")
    public ResponseEntity<UserDTO> getAccountInfo(Principal principal) {
        String principalName = principal.getName();
        Assert.notNull(principalName, "User is not authenticated!");

        UserDTO loggedInUser = userService.getUserByEmail(principalName);
        if (Objects.isNull(loggedInUser)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(loggedInUser);
    }
}
