package com.shamba.records.authservice.config;

import com.shamba.records.authservice.model.enums.UserType;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import java.util.Objects;
import java.util.Optional;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Configuration
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        String userName = SecurityUtils.getCurrentUserLogin();
        return Objects.isNull(userName) ? Optional.of(UserType.System.name()) : Optional.of(userName);
    }
}
