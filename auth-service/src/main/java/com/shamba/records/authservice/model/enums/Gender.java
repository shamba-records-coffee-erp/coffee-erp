package com.shamba.records.authservice.model.enums;

public enum Gender {
    Male, Female
}
