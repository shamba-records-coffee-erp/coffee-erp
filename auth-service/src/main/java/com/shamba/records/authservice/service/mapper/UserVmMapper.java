package com.shamba.records.authservice.service.mapper;

import com.shamba.records.authservice.model.User;
import com.shamba.records.authservice.service.vm.UserVM;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

/**
 * @author Erick Loningo Lomunyak
 * @date 11/12/2021-9:39 PM
 */
@Service("adminMapper")
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserVmMapper extends EntityMapper<UserVM, User> {
}
