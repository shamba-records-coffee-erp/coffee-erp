package com.shamba.records.authservice.service.impl;

import com.shamba.records.authservice.model.Role;
import com.shamba.records.authservice.model.User;
import com.shamba.records.authservice.repository.RoleRepository;
import com.shamba.records.authservice.repository.UserRepository;
import com.shamba.records.authservice.service.UserService;
import com.shamba.records.authservice.service.dto.UserDTO;
import com.shamba.records.authservice.service.exception.IdNoAlreadyExistsException;
import com.shamba.records.authservice.service.exception.PhoneAlreadyExistsException;
import com.shamba.records.authservice.service.exception.UserAlreadyExistsException;
import com.shamba.records.authservice.service.mapper.UserMapper;
import com.shamba.records.authservice.service.mapper.UserVmMapper;
import com.shamba.records.authservice.service.vm.UserVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.security.RandomUtil;

import java.util.HashSet;
import java.util.Set;

@Transactional
@Service("userService")
public class DefaultUserService implements UserService {

    private final UserRepository userRepo;
    private final UserMapper userMapper;
    private final UserVmMapper adminMapper;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public DefaultUserService(UserRepository userRepo, UserMapper userMapper, UserVmMapper adminMapper,
                              RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.userMapper = userMapper;
        this.adminMapper = adminMapper;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> getAllUsers(Pageable pageable) {
        return userRepo.findAll(pageable).map(userMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserById(Long id) {
        return userMapper.toDto(userRepo.findById(id).orElse(null));
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserByEmail(String email) {
        return userMapper.toDto(userRepo.findOneWithRolesByEmailIgnoreCase(email).orElse(null));
    }

    @Override
    public UserDTO createUser(UserVM userVM) {
        userRepo.findFirstByEmailIgnoreCase(userVM.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw  new UserAlreadyExistsException(existingUser.getEmail());
            }
        });

        userRepo.findFirstByPhoneNumber(userVM.getPhoneNumber()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw  new PhoneAlreadyExistsException(existingUser.getPhoneNumber());
            }
        });

        userRepo.findFirstByIdNo(userVM.getIdNo()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw  new IdNoAlreadyExistsException(existingUser.getIdNo());
            }
        });


        User user = adminMapper.toEntity(userVM);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(getRoles(user.getRoles()));
        user.setActivationKey(RandomUtil.generateActivationKey());
        return userMapper.toDto(userRepo.save(user));
    }

    @Override
    public boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.isActivated()) {
            return false;
        }
        userRepo.delete(existingUser);
        userRepo.flush();
        return true;
    }

    private Set<Role> getRoles(Set<Role> uncheckedRoles) {
        Set<Role> finalRoles = new HashSet<>();
        uncheckedRoles.forEach(role -> roleRepository.findFirstByName(role.getName()).ifPresent(finalRoles::add));
        return  finalRoles;
    }
}
