package com.shamba.records.authservice.service.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.spring.web.advice.ProblemHandling;

/**
 * @author Erick Loningo Lomunyak
 */
@ControllerAdvice
public class ExceptionHandler implements ProblemHandling {
}
