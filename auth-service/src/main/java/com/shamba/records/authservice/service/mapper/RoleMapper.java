package com.shamba.records.authservice.service.mapper;

import com.shamba.records.authservice.model.Role;
import com.shamba.records.authservice.service.dto.RoleDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {
}
