package com.shamba.records.authservice.service.mapper;

import com.shamba.records.authservice.model.User;
import com.shamba.records.authservice.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service("userMapper")
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserMapper extends EntityMapper<UserDTO, User> {
}
