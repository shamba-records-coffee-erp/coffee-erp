package com.shamba.records.authservice.config;

import com.shamba.records.authservice.model.Role;
import com.shamba.records.authservice.repository.UserRepository;
import com.shamba.records.authservice.service.impl.UserNotActivatedException;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author Erick Loningo Lomunyak
 * @date 19/12/2021-12:09 AM
 */
@EnableWebSecurity
public class DefaultSecurityConfig {

    private final UserRepository userRepository;

    public DefaultSecurityConfig(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Bean
    @Order(1)
    public SecurityFilterChain jwtSecurityFilterChain(HttpSecurity http, CorsFilter corsFilter) throws Exception {
        http
                .addFilterBefore(corsFilter, CorsFilter.class)
                .antMatcher("/auth/account/**").authorizeRequests().anyRequest().authenticated()
                .and().oauth2ResourceServer().jwt();
        return http.build();
    }

    @Bean
    @Order(2)
    public SecurityFilterChain standardSecurityFilterChain(HttpSecurity http, CorsFilter corsFilter) throws Exception {
        // @formatter:off
        http
                .addFilterBefore(corsFilter, CorsFilter.class)
                .csrf(csrf -> csrf
                        .ignoringAntMatchers("/h2-console/**")
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                )
                .headers().frameOptions().disable()
            .and()
                .authorizeRequests()
                .antMatchers("/management/**", "/h2-console/**").permitAll()
                .anyRequest().authenticated()
            .and()
                .formLogin(withDefaults());
        return http.build();
    }

    @Bean
    @Transactional(readOnly = true)
    public UserDetailsService defaultUserDetailsService() {
        return username -> userRepository.findOneWithRolesByEmailIgnoreCase(username)
                .map(user -> {
                    if (!user.isActivated()) {
                        throw new UserNotActivatedException("User " + username + " was not activated");
                    }
                    return new User(user.getEmail(), user.getPassword(), user.getRoles().stream().map(Role::getName)
                            .map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                })
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Bean
    public PasswordEncoder delegatingPasswordEncoder() {
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());
        DelegatingPasswordEncoder passwordEncoder = new DelegatingPasswordEncoder("bcrypt", encoders);
        passwordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        return passwordEncoder;
    }

    @Bean
    CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.setAllowCredentials(true);
        corsConfig.addAllowedOriginPattern("*");
        corsConfig.addAllowedHeader("*");
        corsConfig.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", corsConfig);
        return new CorsFilter(source);
    }
}
