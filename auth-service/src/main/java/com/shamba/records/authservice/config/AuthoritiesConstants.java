package com.shamba.records.authservice.config;

public final class AuthoritiesConstants {

    // Roles
    public static final String ADMIN = "ROLE_SYSTEM_ADMIN";
    public static final String FARMER = "ROLE_FARMER";
    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    public static final String COOP_USER = "ROLE_COOP_USER";
    public static final String COOP_ADMIN = "ROLE_COOP_ADMIN";
    public static final String COOP_CLERK = "ROLE_COOP_CLERK";
    public static final String WAREHOUSE_USER = "ROLE_WAREHOUSE_USER";
    public static final String WAREHOUSE_ADMIN = "ROLE_WAREHOUSE_ADMIN";
    public static final String WAREHOUSE_CLERK = "ROLE_WAREHOUSE_CLERK";

    // Permissions
    public static final String CREATE_PEM = "create";
    public static final String READ_PEM = "read";
    public static final String WRITE_PEM = "write";
    public static final String UPDATE_PEM = "update";
    public static final String DELETE_PEM = "delete";

}
