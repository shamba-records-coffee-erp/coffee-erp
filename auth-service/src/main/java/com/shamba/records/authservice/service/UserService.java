package com.shamba.records.authservice.service;

import com.shamba.records.authservice.model.User;
import com.shamba.records.authservice.service.dto.UserDTO;
import com.shamba.records.authservice.service.vm.UserVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    Page<UserDTO> getAllUsers(Pageable pageable);

    UserDTO getUserById(Long id);

    UserDTO getUserByEmail(String email);

    UserDTO createUser(UserVM user);

    boolean removeNonActivatedUser(User existingUser);
}
