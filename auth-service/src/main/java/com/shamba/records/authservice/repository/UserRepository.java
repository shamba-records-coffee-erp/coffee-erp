package com.shamba.records.authservice.repository;

import com.shamba.records.authservice.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User>  findFirstByEmailIgnoreCase(String email);
    Optional<User>  findFirstByPhoneNumber(String phone);
    Optional<User>  findFirstByIdNo(String idNo);

    @EntityGraph(attributePaths = {"roles"})
    Optional<User> findOneWithRolesByEmailIgnoreCase(String email);
}
